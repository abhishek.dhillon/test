import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';
import { student_schema, student} from '../student.model';
import { MatTableDataSource } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  dataSource: any;
  columnsToDisplay = ["Full_Name", "Email ID", "Enrollment Number", "Gender", "Phone Number"];
  constructor(private appService: AppService, http: HttpClient) {
    this.dataSource = new MatTableDataSource([]);
   }
   @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

   getStudentData()
   {
     this.appService.getStudentFormData().subscribe( req => this.dataSource.data = req);
   }

  ngOnInit(): void {
    this.getStudentData();
    this.dataSource = new MatTableDataSource<student_schema>(this.dataSource.data)
    this.dataSource.paginator = this.paginator;
  }

}
