import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NextComponent } from './next/next.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { RouterModule, Routes } from '@angular/router';

import { StudentsComponent } from './students/students.component';
import { DynamicSearchComponent } from './dynamic-search/dynamic-search.component';
import { CombinedSearchComponent } from './combined-search/combined-search.component';
import { AddStudentComponent } from './add-student/add-student.component';

import { MatSelectModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material'  
import { MatPaginatorModule } from '@angular/material';
import { ImageComponent } from './image/image.component';

const appRoutes: Routes = [
  { path: 'next', component: NextComponent },
  { path: 'combinedSearch', component: CombinedSearchComponent },
  { path: 'dynamicSearch', component: DynamicSearchComponent },
  { path: 'students', component: StudentsComponent },
  { path: 'add', component: AddStudentComponent },
  { path: 'image', component: ImageComponent },
  { path:  '', redirectTo:  '/next', pathMatch:  'full'},
]; 
@NgModule({
  declarations: [
    AppComponent,
    NextComponent,
    StudentsComponent,
    DynamicSearchComponent,
    CombinedSearchComponent,
    AddStudentComponent,
    ImageComponent,
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,MatSelectModule, MatFormFieldModule, MatInputModule , MatTableModule, MatPaginatorModule,
    NgMultiSelectDropDownModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
