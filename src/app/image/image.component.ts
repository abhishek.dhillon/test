import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';
import { saveAs } from 'file-saver';
import * as JSZip from 'jszip';
import { forkJoin } from "rxjs";
@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
   // imageurl = "https://vodafone-pdr.s3.amazonaws.com/image_2019_10_18T13_48_47_814Z.png";
  // imageurl = "https://i.pinimg.com/originals/c7/6d/a9/c76da9f37d9fccd5a8ef4ec48b5ba2de.jpg"
  data:any;
  constructor(private appService: AppService, http: HttpClient) { }
  getRequests = [];
  // downloadZip()
  // {
  //   const zip = new JSZip();
  //   zip
  //       .generateAsync({ type: "blob" })
  //       .then(blob => saveAs(blob, "image.zip"));
  // }
  downloadZip() 
  {
    // var zip = new JSZip();
    var blob = new Blob([this.imageurl]);
    console.log(blob);
    saveAs(blob);
  }
  // downloadZip() 
  // {
  //   this.appService.loadSvgData("https://vodafone-pdr.s3.amazonaws.com/image_2019_10_18T13_48_47_814Z.png",
  //     this.saveAsZip);
  // }

  // private loadSvgData(url: string, callback: Function): void 
  // {
  //   this.appService.getImage(url).subscribe(req =>
  //     callback(req))
  // }

  // private saveAsZip(content: Blob): void {
  //   var zip = new JSZip.default();
  //   zip.file("image.svg", content);
  //   zip.generateAsync({ type: "blob" })
  //     .then(blob => saveAs(blob, 'image.zip'));
  // };
  getImage()
  {
    this.appService.getImage(this.imageurl).subscribe(req => {
      console.log(req);
      this.data = req;
    })
  }
  ngOnInit(): void 
  {
    this.getImage();
  }
}

