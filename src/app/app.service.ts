import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { switchMap, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) {}
  data:any;
  query:string;

  loadSvgData(url: string, callback: Function) : void{
    this.http.get(url, { responseType: "arraybuffer" })
             .subscribe(x => callback(x));
  }

  getImage(url)
  {
    let headers = new HttpHeaders();

    headers = headers.set('Accept', 'application/pdf');
    return this.http.get(url, { headers: headers, responseType: 'blob' });
  }

  getDynamicData(query): Observable<any>
  {
    return this.http.get('http://localhost:8006/search/?x='+ query);
  }

  getGenderData(query): Observable<any>
  {
    return this.http.get('http://localhost:8006/combined_search/?x='+ query);
  }

  getStudentFormData(): Observable<any>
  {
    return this.http.get('http://localhost:8006/form');
  }
}
