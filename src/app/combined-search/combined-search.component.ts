import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';
import { student_schema, student } from '../student.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-combined-search',
  templateUrl: './combined-search.component.html',
  styleUrls: ['./combined-search.component.css']
})
export class CombinedSearchComponent implements OnInit {

  result = new student
  InputName: String;
  columnsToDisplay = ["Full_Name", "Email ID", "Enrollment Number", "Gender", "Phone Number"];
  constructor(private appService: AppService, http: HttpClient) {
  }

  ngOnInit(): void {
  }
  CombinedSearch() {
    console.log(this.result.query);
    this.appService.getGenderData(JSON.stringify(this.result.query)).subscribe(req => this.result.combined_search_data = req);
  }
}
