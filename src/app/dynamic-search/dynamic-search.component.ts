import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';
import { student_schema, student } from '../student.model';


@Component({
  selector: 'app-dynamic-search',
  templateUrl: './dynamic-search.component.html',
  styleUrls: ['./dynamic-search.component.css']
})
export class DynamicSearchComponent implements OnInit {

  result = new student
  InputName: String;
  columnsToDisplay = ["Full_Name", "Email ID", "Enrollment Number", "Gender", "Phone Number"];
  constructor(private appService: AppService, http: HttpClient) {
  }

  ngOnInit(): void {
  }
  Dynamic_Search_fn() {
    if (this.InputName.length > 3) {
      this.appService.getDynamicData(this.InputName).subscribe(req => this.result.name_fetched_data = req);
    }
  }
}
