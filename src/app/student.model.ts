
export interface student_schema {
    fullname: String
    email: String
    rollno: Number
    Gender: String
    phone_no: Number
}


export class student {
    public student_data :student_schema[]
    public name_fetched_data :student_schema[]
    public combined_search_data :student_schema[]
    public gender:Array<any> = [
        {id: "M", name: "Male"},
        {id: "F", name: "Female"}
    ];
    public query = {fullname: "", Gender: ""}
}