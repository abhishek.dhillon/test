import { Component, ViewChild } from '@angular/core'
import { AppService } from './app.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = "My MEAN STACK App"

  constructor(private appService: AppService, http: HttpClient) {
  }
  ngOnInit() {
  }
}
